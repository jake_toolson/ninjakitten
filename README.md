# NinjaCat Coding Exercise

The goal of this test is to show us which techniques, styles, patterns and practices you use when developing web applications, not how much you can code. You can use any online resources (Google, Stack Overflow, etc.) to complete the test. If anything is unclear and you require any clarifications on the requirements, do not hesitate to email us at <mailto:randar@ninjacat.io>.

Once you are done, email the source code of the solution or a link to it online (e.g. Dropbox) to randar@ninjacat.io. For simplicity, our preference is to run this under "php artisan serve" or Apache, but if there are any dependencies that we need to configure, please indicate that in the email. We will confirm once we receive it. If we believe it is acceptable, we will schedule a follow up interview. We we meet, plan to spend around 15 minutes walking us through your code explaining what you did and why.

## Guidelines
- Treat this like production code. Although you will be judged on how much you get done, we will be looking more at the quality, craftsmanship and creativity of what you have delivered. 
- Treat this like a shippable product. 
 - The first thing we are going to do is start the application and see if it works (black box testing). A non-working application will be heavily penalized.
 - The second thing we are going to do is look at the quality of the code in terms of design, cleanliness, maintainability and performance (white box testing).
- Do not change the schema or data in the database. When we test your code, we will connect it to an existing database. 
- Provide instructions on how to setup the application. For example:
 - Where to set the connection string.
 - Any package managers that need to be run  
- If there is something you think should be done, but you don’t have time, please add a TODO comment. E.g.
 - <!-TODO: Should add better error handling here -->
 - // TODO: This is a performance bottleneck and I would normally use XYZ

 
# Step 1 - Create a Single Page Application
The HTML provided is based on a free Bootstrap template. The home page opens to a dashboard with an area chart. The data is currently static. 

## Goals
- Retrieve the data from a MySQL database that can be generated from database/test_db.sql
- Have the dropdown return all the data or filter it by year

## Technical guidelines
- The frontend can use any frameworks you wish, with a preference for vue.js, React or AngularJS
 - The HTML code is poorly structured. There is significant copy and paste from the Boostrap template. Modularize the page.
 - Feel free to replace thechart  control if you don't want to use the morris.js chart
- The backend must be PHP but you are free to use any frameworks you wish with a preference for Laravel.

# Step 2 - Additional Item
Pick **one** of the following.

## Dynamic Table from SQL
Under tables, there is a sample HTML table. Instead of the static data provided, return the data from the clicks table grouped by the provider. Feel free to use any control you wish to render the data. The UI is only provided as a reference.

## Clicks data from Mongo
Under tables, there is a sample HTML table. Instead of the static data provided, pull the data from MongoDB.
- Come up with a schema that would be appropriate for tracking click for a busy system.
- Include some sample Mongo data we can import and include instructions. e.g. using mongoimport. 

## User Management and login
There is a sample login page. Create a user management system and login. If you chose to do this option you will need to change the database schema. Provide a SQL script to update the current database and add any additional tables. Provide instructions on how the first user can access the system. 
