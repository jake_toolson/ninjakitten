## Iteration 1
I am not a fan of having the API return data formatted specifically to fit, in this case, a chart.  It doesn't feel RESTful nor very maintainable/scalable.

Originally, I created a crosstab query whose SQL, after generated using fluent, looked like this:

```sql
Select 
QUARTER(date) as quarter,
CONCAT(YEAR(date), " Q", QUARTER(date)) as period,
YEAR(date) as year,
SUM(CASE WHEN provider.name = 'adwords' then total_clicks end) as adwords,
SUM(CASE WHEN provider.name = 'bing' then total_clicks end) as bing,
SUM(CASE WHEN provider.name = 'facebook' then total_clicks end) as facebook
FROM clicks
LEFT JOIN provider ON provider_id = provider.id
GROUP BY period
```

Which formatted to JSON produced the following results:
```JSON
[
    {
        quarter: 1,
        period: "2015 Q1",
        year: 2015,
        adwords: 15784,
        bing: 10269,
        facebook: 14917
    },
    {
        quarter: 2,
        period: "2015 Q2",
        year: 2015,
        adwords: 8009,
        bing: 13180,
        facebook: 8600
    }
]

```

This worked perfect for morris.js however I wanted to create something more fluent, and dynamic.

What about a custom query object that builds the query based on parameters and the front-end builds the data format.
What about GraphQL?

----
## Iteration 2

My 2nd iteration consisted of me creating a basic API response with all data.  The frontend had the burden of consuming 
transforming it to fit the datatable and charts.

This was fine however I needed more ability to control what I want to retrieve from the API.  

----
## Iteration 3

Final iteration I created the ability to parse the query parameters from the url and apply them to the
results query.  This gave me more of an opportunity to control the results for each different implementation, and didn't 
require the backend to create specific endpoints and formats for my use-cases;

Just needed to pass the query parameters I wanted for each implementation and it was easy.

----
View ninjakitten.raml for API documentation

Sample API Routes:
http://127.0.0.1:8000/api/clicks
http://127.0.0.1:8000/api/clicks?year=2016,2017
http://127.0.0.1:8000/api/clicks?group_by=period,provider_id&sum=total_clicks&year=2016
