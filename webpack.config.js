const path = require('path');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const CleanWebpack = require('clean-webpack-plugin');
const ExtractText = require('extract-text-webpack-plugin');
const UglifyJS = require('uglifyjs-webpack-plugin');

const assetsDir = `${__dirname}/resources/assets/`;

module.exports = {
    entry: {
        'js/app' : [
            assetsDir + 'js/main.js',
        ],
        'js/vendor' : [
            assetsDir + 'js/vendor.js',
        ],
        'css/vendor' : [
            assetsDir + 'scss/vendor.scss',
        ],
        'css/app' : [
            assetsDir + 'scss/app.scss',
        ]
    },
    output: {
        path: path.join( __dirname, 'public', 'build'),
        filename: "[name]-[hash].js",
        publicPath: 'public/build'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'env']
                    }
                },
                include: [
                    /vue2-datatable-component/
                ]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ExtractText.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true
                            }
                        },
                        'sass-loader',
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
        ]
    },
    plugins: [
        new UglifyJS(),
        // Generates file with hashed build names
        new WebpackAssetsManifest({
            output: 'rev-manifest.json',
        }),
        new ExtractText({
            filename: '[name]-[hash].css',
        }),
        // This cleans out previous build files
        new CleanWebpack(
            ['js', 'css'],
            {
                root: path.join(__dirname, 'public', 'build'),
                exclude: ['components']
            }
        ),
    ],
    resolve: {
        // Resolves files automatically with these extensions
        extensions: ['.js', '.jsx', '.vue', '.json'],
        alias: {
            'vue': 'vue/dist/vue.js',
            '@': path.resolve('resources/assets/js'),
        }
    },
};