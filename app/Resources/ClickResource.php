<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Resources;

use App\Models\Click;
use Illuminate\Http\Resources\Json\Resource;

class ClickResource extends Resource
{
    /**
     * @var Click
     */
    public $resource;

    public function toArray($request): array
    {
        $mapped = [
            'id' => $this->resource->id,
            'year' => $this->resource->year,
            'quarter' => $this->resource->quarter,
            'period' => $this->resource->period,
            'total_clicks' => $this->resource->total_clicks,
            'date' => $this->resource->date,
            'provider' => $this->resource->provider,
        ];

        return array_merge(array_diff($this->resource->getAttributes(), $mapped), $mapped);
    }
}