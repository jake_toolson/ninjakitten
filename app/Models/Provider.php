<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string name
 * @property string display_name
 *
 * @property Collection|Click[] clicks
 */
class Provider extends Model
{
    protected $table = 'provider';

    protected $fillable = [
        'name',
        'display_name',
    ];

    public function clicks(): HasMany
    {
        return $this->hasMany(Click::class, 'provider_id');
    }
}
