<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Models\Query;

use DB;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

// TODO: Create interface that defines the apply method
// TODO: Create base method which defines construct and getFilters so other classes can use these resources.
class ClickFilter implements QueryInterface
{
    private $filters;

    private $allowed = [
        'group_by' => [
            'period',
            'provider_id',
            'date',
            'year_month',
        ]
    ];

    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    // TODO: Create method based handlers for each filter instead of defining them inside apply().
    public function apply(Builder $builder): Builder
    {
        $filters = $this->getFilters();

        if (array_key_exists('sum', $filters)) {
            $values = $filters['sum'];
            foreach ($values as $value) {
                // Note: aliasing the value being summed back to the original name... could have unintended side effects
                // TODO: Identify better way :)
                $builder->select()->addSelect(DB::raw("sum({$value}) as {$value}"));
            }
        }

        if (array_key_exists('year', $filters)) {
            $placeholders = rtrim(str_repeat('?,', count($filters['year'])), ',');
            $builder->whereRaw("YEAR(`date`) IN ({$placeholders})", $filters['year']);
        }

        if (array_key_exists('group_by', $filters)) {
            $values = $filters['group_by'];

            foreach ($values as $value) {
                if (in_array($value, $this->allowed['group_by'], true)) {
                    $method = 'groupBy'.Str::camel($value).'Resolver';
                    if (method_exists($this, $method)) {
                        $this->{$method}($builder);
                    } else {
                        $builder->groupBy($value);
                    }
                }
            }
        }

        if (array_key_exists('sort', $filters)) {
            $order = array_key_exists('order', $filters) ? $filters['order'][0] : 'desc';
            foreach ($filters['sort'] as $sort) {
                $builder->orderBy($sort, $order);
            }
        }

        return $builder;
    }

    private function groupByPeriodResolver(Builder $builder): void
    {
        $builder->groupBy(DB::raw('CONCAT(YEAR(`date`), " Q", QUARTER(`date`))'));
    }

    private function groupByYearMonthResolver(Builder $builder): void
    {
        $builder->groupBy(DB::raw('CONCAT(YEAR(`date`), MONTH(`date`))'));
    }
}