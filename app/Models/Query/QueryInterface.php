<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Models\Query;

use Illuminate\Database\Eloquent\Builder;

interface QueryInterface
{
    public function apply(Builder $builder): Builder;
}
