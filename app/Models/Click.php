<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Models;

use App\Models\Query\QueryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property int provider_id
 * @property string date
 * @property int total_clicks
 * @property string quarter
 * @property string period
 * @property string year
 *
 * @property Provider provider
 */
class Click extends Model
{
    protected $table = 'clicks';

    protected $appends = [
        'year',
        'quarter',
        'period'
    ];

    protected $fillable = [
        'date',
        'total_clicks',
    ];

    protected $casts = [
        'total_clicks' => 'int'
    ];

    public function getYearAttribute(): int
    {
        return (int) date('Y', strtotime($this->date));
    }

    public function getQuarterAttribute(): int
    {
        return (int) ceil(date('m', strtotime($this->date))/3);
    }

    public function getPeriodAttribute(): string
    {
        return $this->year . '-Q' . $this->quarter;
    }

    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    // TODO: This should be implemented in a custom EloquentQueryBuilder class so other classes can have custom queries;
    public static function buildQuery(QueryInterface $filter): Builder
    {
        $query = static::query();

        return $filter->apply($query);
    }
}
