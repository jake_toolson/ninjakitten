<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Query\QueryParser;
use App\Models\Click;
use App\Models\Query\ClickFilter;
use App\Resources\ClickResource;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ClickApiController extends Controller
{
    public function index(Request $request)
    {
        // TODO: Use a custom paginator without relying on laravel paginator/structure.
        // TODO: Pass a filter/builder interface to a repository or data layer for building queries
        /** @var LengthAwarePaginator $query */
        $query = Click::buildQuery($this->buildClickFilter($request))
            ->with('provider')
            ->paginate($request->get('limit', 100))
            ->appends($request->query());

        // TODO: Create a custom resource module for transforming collection and entities.
        return ClickResource::collection($query);
    }

    public function show(Request $request, int $clickId)
    {
        return new ClickResource(Click::buildQuery($this->buildClickFilter($request))
            ->with('provider')
            ->findOrFail($clickId));
    }

    private function buildClickFilter(Request $request): ClickFilter
    {
        $query = new QueryParser($request->all());

        return new ClickFilter($query->getFilters());
    }
}
