<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

namespace App\Http\Controllers\Query;

class QueryParser
{
    /**
     * @var array|null
     */
    private $parameters;

    public function __construct(array $parameters = null)
    {
        $this->parameters = $parameters;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getFilters(): array
    {
        return $this->parseFilters($this->getParameters())->get();
    }

    // TODO: Move this to a trait so other query parsing classes can reuse the logic.
    // TODO: Identify recursive method.
    protected function parseFilters(array $parameters = null): FiltersParameter
    {
        $filtersParameter = new FiltersParameter;

        $filters = array_filter($parameters);
        foreach ($filters as $filterName => $filterValues) {
            $fields = array_filter(array_map('trim',  explode(',', $filterValues)));
            if ($fields) {
                foreach ($fields as $field) {
                    $filtersParameter->add($filterName, $field);
                }
            }
        }

        return $filtersParameter;
    }
}