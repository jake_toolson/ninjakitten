const mixins = {
    methods: {
        groupDataByKeys: (data, cb) => {
            let groups = {};

            data.forEach((o) => {
                let group = JSON.stringify(cb(o));
                groups[group] = groups[group] || [];
                groups[group].push(o);
            });

            return Object.keys(groups).map(function (group) {
                return groups[group];
            })
        },
        getUniqueByName: (data, cb) => {
            return _.uniqWith(data.map(item => cb(item)), _.isEqual);
        },
        formatClicksDataToMorris: (data) => {
            let mappedByQuarter = mixins.methods.groupDataByKeys(data, item => [item.period]);
            let results = [];

            for (var quarters of mappedByQuarter) {
                let object = {};
                for (var dataSet of quarters) {
                    object.period = dataSet.period.replace('-', ' ');
                    object[dataSet.provider.name] = dataSet.total_clicks;
                }
                results.push(object);
            }

            return results;
        }
    },
};
// TODO: Export the type of mixins (filters, computed, etc)
export default mixins;