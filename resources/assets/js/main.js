/*
 * Copyright (c) 2018. Jake Toolson
 */

import Datatable from 'vue2-datatable-component'
import Vue from 'vue';
import router from '@/router'
import App from '@/App';

Vue.use(Datatable);

import Raphael from 'raphael/raphael'
global.Raphael = Raphael;

new Vue({
    el: '#root',
    router,
    render: (h) => {
        return h('App')
    },
    components: {
        App
    }
});
