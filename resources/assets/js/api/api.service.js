/*
 * Copyright (c) 2018. Jake Toolson
 */

import http from '@/api/axios'
import { getRoutePathByName } from '@/router';

const ApiService = {
    init () {},
    query (resource, params) {
        return http.get(resource, {params: params})
            .catch((error) => {
                throw new Error(`ApiService ${error}`)
            })
    },
    get (resource) {
        return http.get(resource)
            .catch((error) => {
                throw new Error(`ApiService ${error}`)
            })
    },
};

export default ApiService;

export const ClicksService = {
    all () {
        return ApiService.get(getRoutePathByName('api.clicks'));
    },
    query (params) {
        return ApiService.query(getRoutePathByName('api.clicks'), params);
    },
};