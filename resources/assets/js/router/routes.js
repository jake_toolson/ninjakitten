/*
 * Copyright (c) 2018. Jake Toolson
 */

import Home from '@/views/Home';
import Tables from '@/views/Tables';

export default [
    {
        path: '/',
        component: Home,
        name: 'home',
    },
    {
        path: '/tables',
        component: Tables,
        name: 'tables',
    },
    {
        path: '/api',
        children : [
            {
                path: 'clicks',
                name: 'api.clicks'
            },
            {
                path: 'clicks/:id',
                name: 'api.clicks.show'
            },
        ],
    },
    {
        path: '*',
        redirect: { name: 'home' }
    }
];