import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from '@/router/routes';

Vue.use(VueRouter);

let router = new VueRouter({
    mode: 'history',
    routes: routes,
});

export default router;

export const getRoutePathByName = (name, params) => {
    const route = router.resolve({
        name: name,
        params: params
    });

    return route ? route.href : null;
};