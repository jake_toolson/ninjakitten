<?php
/**
 * Copyright (c) 2018. Jake Toolson
 */

use Illuminate\Database\Seeder;

class LoadProviders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert(DB::raw("INSERT INTO `provider` VALUES (1,'adwords','Google AdWords'),(2,'bing','Bing'),(3,'facebook','Facebook'),(4,'yahoo_gemini','Yahoo Gemini'),(5,'analytics','Google Analytics'),(6,'search_console','Gooogle Search Console'),(7,'twitter','Twiter Ads'),(8,'yelp','Yelp'),(9,'adroll','AdRoll'),(10,'mongoose','Mongoose Metrics'),(11,'sheets','Google Sheets');"));
    }
}
