<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClicksTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clicks', function (Blueprint $table){
            $table->increments('id');
            $table->integer('provider_id', false, true);
            $table->date('date')->nullable()->default(null);
            $table->string('total_clicks', 45)->nullable()->default(null);

            $table->index('provider_id', 'provider_fk_idx');
            $table->foreign('provider_id', 'provider_fk')
                ->references('id')
                ->on('provider')
                ->onDelete('NO ACTION')
                ->onUpdate('NO ACTION');
        });

        DB::statement("ALTER TABLE clicks CONVERT TO CHARACTER SET latin1 COLLATE latin1_swedish_ci;");
        DB::statement("ALTER TABLE clicks AUTO_INCREMENT = 83;");
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clicks');
    }
}
