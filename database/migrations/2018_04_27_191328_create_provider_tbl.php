<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider', function (Blueprint $table){
            $table->increments('id');
            $table->char('name', 45);
            $table->char('display_name', 45);
            $table->unique('name', 'NAME_IX');
        });

        DB::statement("ALTER TABLE provider CONVERT TO CHARACTER SET latin1 COLLATE latin1_swedish_ci;");
        DB::statement("ALTER TABLE provider AUTO_INCREMENT = 12;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider');
    }
}
